function cari(arr){
    let criArray = [];

    let minNum = Math.min(...arr);
    let maxNum = Math.max(...arr);

    for(let i = minNum; i<maxNum; i++){
        if(arr.indexOf(i) < 0){
            criArray.push(i);
        }
    }
    return criArray;
}

console.log(cari([1, 2, 4, 5, 6, 7, 9]));