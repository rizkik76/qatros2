// class BilanganPrima {
//   result = [];

//   constructor(n) {
//       this.n = n;
//   }

//   prima() {
//       for (let i = 2; i < this.n; i++) {
//           let flag = 0;

//           for (let j = 2; j < i; j++) {
//               if (i % j == 0) {
//                   flag = 1;
//                   break;
//               }
//           }

//           if (i > 1 && flag == 0) {
//               this.result.push(i);
//           }
//       }

//       return this.result;
//   }
// }

// const data = new BilanganPrima(15);
// console.info(data.prima())
// console.log(BilanganPrima.length);


// Javascript program to find sum of
// primes in given array.

// Mencari bilprim
function BilanganPrima(arr, n) {
	let max_val = arr.sort((a, b) => b - a)[0];

	let prime = new Array(max_val + 1).fill(true);

	prime[0] = false;
	prime[1] = false;
	for (let p = 2; p * p <= max_val; p++) {

		if (prime[p] == true) {
			for (let i = p * 2; i <= max_val; i += p)
				prime[i] = false;
		}
	}

	// menjumlah semua bilprim
	let sum = 0;
	for (let i = 0; i < n; i++)
		if (prime[arr[i]])
			sum += arr[i];

	return sum;
}

let arr = [1, 2, 3, 4, 5, 6, 7];
let n = arr.length;

console.log(BilanganPrima(arr, n));
